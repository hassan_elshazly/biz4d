<?php

return [
    /**
     * Control if the seeder should create a user per role while seeding the data.
     */
    'create_users' => false,

    /**
     * Control if all the laratrust tables should be truncated before running the seeder.
     */
    'truncate_tables' => true,

    'roles_structure' => [
        'superadministrator' => [
            'users' => 'c,r,u,d,l',
            "manager"=>"c,r,u,d,l",
            "projects"=>"c,r,u,d,l",
            'profile' => 'r,u'
        ],
        'manager' => [
            'profile' => 'r',
            "projects"=>'l',
        ],
        'user' => [
            'profile' => 'r',
            'projects' => 'c,r,u,l',
        ],

    ],

    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
