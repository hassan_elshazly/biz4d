<div class="container mt-3">
    <div class="form-group row">
        <div class="col-lg-6">
            <label>{{__('Name')}}<span class="text-danger">*</span></label>
            {!! Form::text('name', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
            @if ($errors->has("name"))
                <span class="form-text text-danger">{{$errors->first('name')}}</span>
            @endif
        </div>
        <div class="col-lg-6">
            <label>{{__('Description')}}<span class="text-danger">*</span></label>
            {!! Form::text('desc', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
            @if ($errors->has("desc"))
                <span class="form-text text-danger">{{$errors->first('desc')}}</span>
            @endif
        </div>
        <div class="col-lg-6">
            <label>{{__('Project URL')}}<span class="text-danger">*</span></label>
            {!! Form::text('url', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
            @if ($errors->has("url"))
                <span class="form-text text-danger">{{$errors->first('url')}}</span>
            @endif
        </div>
        <div class="col-lg-6">
            <label>{{__('Status')}}<span class="text-danger">*</span></label>
            {!! Form::select('status', $status,null, ['class'=>'form-control','placeholder'=>"Select status" ,'disabled'=>$disabled]) !!}
            @if ($errors->has("status"))
                <span class="form-text text-danger">{{$errors->first('status')}}</span>
            @endif
        </div>
        @if(!$disabled)
            <div class="col-lg-6">
                <label>{{__('Image')}} @if(!$preview) <span class="text-danger">*</span> @endif</label>
                {!! Form::file('image', ['class'=>'form-control', 'disabled'=>$disabled,"accept"=>"image/*"]) !!}
                @if ($errors->has("image"))
                    <span class="form-text text-danger">{{$errors->first('image')}}</span>
                @endif
            </div>
            <div class="col-lg-6">
                <label>{{__('Video')}} @if(!$preview) <span class="text-danger">*</span> @endif </label>
                {!! Form::file('video', ['class'=>'form-control', 'disabled'=>$disabled,"accept"=>"video/*"]) !!}
                @if ($errors->has("video"))
                    <span class="form-text text-danger">{{$errors->first('video')}}</span>
                @endif
            </div>
            <div class="col-lg-6">
                <label>{{__('Document')}}</label>
                {!! Form::file('doc', ['class'=>'form-control', 'disabled'=>$disabled,"accept"=>".xlsx, .xls, .pdf,.csv,.docx"]) !!}
                @if ($errors->has("doc"))
                    <span class="form-text text-danger">{{$errors->first('doc')}}</span>
                @endif
            </div>
        @endif
        @if($preview)
            @foreach ($project->files as $file)
            <div class="col-lg-6">
                 @if($file->mime == "image")
                        <div class="image">
                            <label for="image">{{__("image")}}</label><br>
                            <img src="{{$file->base . $file->name}}" alt="" class="img-thumbnail" id="image" height="250" width="400">
                        </div>
                    @elseif ($file->mime == "video")
                        <div class="video">
                            <label for="vieo">{{__("video")}}</label><br>
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{$file->base . $file->name}}" allowfullscreen ></iframe>
                            </div>
                        </div>
                    @else
                        <div class="doc">
                            <label for="doc">{{__("Document")}}</label><br>
                            <a href="{{$file->base . $file->name}}" class="btn btn-primary">{{__("Download Document!")}}</a>
                        </div>
                @endif
            </div>
            @endforeach


        @endif
    </div>
</div>
