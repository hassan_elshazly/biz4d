@extends('layouts.app', ['title' => $title ?? ''])

@section('content')

<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="background-size: cover; background-position: center top;">
    <!-- Mask -->
    <span class="mask bg-gradient-default opacity-8"></span>
</div>


<div class="container-fluid mt--7">
        <div class="card bg-secondary shadow">
            <div class="kt-portlet__body">
                {!! Form::open(['route'=>'project.store', 'method'=>'post', 'class'=>'kt-form kt-form--labe-right','files'=>true]) !!}
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        @include("layouts.includes.form_button",['disabled'=>false,"route"=>"project"])
                    </div>
                    @include('projects.form', ['disabled'=>false,'preview'=>false])
                {!! Form::close() !!}
            </div>
        </div>
</div>


@endsection
