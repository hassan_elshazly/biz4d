@extends('layouts.app', ['title' => $title ?? ''])

@section('content')

<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="background-size: cover; background-position: center top;">
    <!-- Mask -->
    <span class="mask bg-gradient-default opacity-8"></span>
</div>


    <div class="container-fluid mt-2">
        @include("layouts.includes.messages")
            <div class="card bg-secondary shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{__("Projects")}}</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{route("project.create")}}" class="btn btn-sm btn-primary">{{__("Create Project")}}</a>
                        </div>
                    </div>
                </div>
                <div style="padding: 10px">
                    <div class="table-responsive p-10">
                        <div class="table-responsive">
                            {!! $dataTable->table([
                                'class'=>'table align-items-center table-flush'
                            ],true)!!}
                        </div>
                    </div>
                </div>
            </div>
    </div>


@endsection

@push('js')

    {!! $dataTable->scripts() !!}

    <!-- Datatables buttons -->
    <script src="{{asset("plugins/datatables/checkbox.js")}}"></script>
    <script>
        delete_all();
    </script>
    <script src="{{asset("plugins/datatables/jquery.dataTables.min.js")}}"></script>
    <script src="{{asset("plugins/datatables-bs4/js/dataTables.bootstrap4.min.js")}}"></script>
    <script src="{{asset("plugins/datatables-responsive/js/dataTables.responsive.min.js")}}"></script>
    <script src="{{asset("plugins/datatables-responsive/js/responsive.bootstrap4.min.js")}}"></script>
    <script src="{{asset("plugins/vendor/datatables/buttons.server-side.js")}}"></script>
    <script src="{{asset("plugins/datatables-buttons/dataTables.buttons.min.js")}}"></script>
    <script src="{{asset("plugins/vendor/datatables/buttons.server-side.js")}}"></script>

@endpush
@push('css')
    <link rel="stylesheet" href="{{asset("plugins/datatables-buttons/css/buttons.bootstrap4.min.css")}}">
    <link rel="stylesheet" href="{{asset("plugins/datatables-responsive/css/responsive.bootstrap4.min.css")}}">
    <link rel="stylesheet" href="{{asset("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css")}}">
@endpush
