<div class="container mt-3">
    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
        <label class="form-control-label" for="input-name">{{ __('Name') }}</label>
        {!! Form::text('name', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}

        @if ($errors->has('name'))
            <span class="form-text text-danger" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('phone') ? ' has-danger' : '' }}">
        <label class="form-control-label" for="input-name">{{ __('Phone') }}</label>
        {!! Form::text('phone', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}

        @if ($errors->has('phone'))
            <span class="form-text text-danger" role="alert">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
        <label class="form-control-label" for="input-email">{{ __('Email') }}</label>
        {!! Form::email('email', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
        @if ($errors->has('email'))
            <span class="form-text text-danger" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
        <div class="input-group input-group-alternative">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
            </div>
            {!! Form::password('password', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
        </div>
        @if ($errors->has('password'))
            <span class="form-text text-danger" style="display: block;" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group">
        <div class="input-group input-group-alternative">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
            </div>
            {!! Form::password('password_confirmation', null, ['class'=>'form-control', 'disabled'=>$disabled]) !!}
        </div>
    </div>
</div>
