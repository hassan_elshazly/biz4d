
<a href="{{ route('user.edit', ['user' => $id]) }}" class="btn btn-sm btn-success btn-icon ">
    <i class=" fa fa-edit"></i>
</a>
<a href="{{route('user.show',['user'=>$id])}}"  target='_blank' class="btn btn-sm btn-primary btn-icon ">
    <i class="ni ni-badge text-white"></i>
</a>
<!-- Trigger the modal with a button -->
<a href="javascript:;" class="btn btn-sm btn-danger btn-icon" title="Delete" data-toggle="modal"
    data-target="#myModal{{ $id }}">
    <i class="fa fa-trash"></i>
</a>


<!-- Modal -->
<div id="myModal{{ $id }}" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">@lang("Delete Record")</h4>
            </div>
            {!! Form::open(['route' => ['user.destroy', $id], 'method' => 'delete']) !!}
            <div class="modal-body">
                <p>{{ __('Are you sure you want delete this item  ? ') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">@lang("Close")</button>
                {!! Form::submit(__('Yes'), ['class' => 'btn btn-danger']) !!}
            </div>
        </div>

    </div>
</div>
