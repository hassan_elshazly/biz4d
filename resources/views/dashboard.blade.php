@extends('layouts.app')

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-7 mb-5 mb-xl-0">
                <div class=" bg-gradient-default shadow">
                    <div class="card-header bg-transparent">
                        <div class="row align-items-center">
                            <div class="col">
                                <h6 class="text-uppercase text-light ls-1 mb-1">{{__("Pie Chart Overview")}}</h6>
                            </div>
                            <div id="piechart" style="width: 900px; height: 500px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-5">
                <div class="card shadow">
                    <div class="card-header bg-transparent">
                        <div class="row align-items-center">
                            <div class="col">
                                <h6 class="text-uppercase text-muted ls-1 mb-1">{{__("Modules dates of creation")}}</h6>
                            </div>
                            <div id="barchart_material" style="width: 900px; height: 500px;"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var status=<?php echo  json_encode($status) ?>;
        var data = google.visualization.arrayToDataTable(status);

        var options = {
          title: 'Projects status',
          slices: {
            0: { color: '#C41F69' },
            1: { color: '#0097E2' },
            2: { color: '#ff49A1' },
            2: { color: '#FF521E' }
          }
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>
  <script type="text/javascript">
    google.charts.load('current', {'packages':['bar']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
      var modules=<?php echo  json_encode($modules) ?>;
      var data = google.visualization.arrayToDataTable(modules);

      var options = {
        bars: 'vertical' // Required for Material Bar Charts.
      };

      var chart = new google.charts.Bar(document.getElementById('barchart_material'));

      chart.draw(data, google.charts.Bar.convertOptions(options));
    }
  </script>
@endpush
