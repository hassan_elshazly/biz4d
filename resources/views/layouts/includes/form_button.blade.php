<div class="kt-portlet__head-toolbar p-2">
    <a href="{{route($route.".index")}}" class="btn btn-primary kt-margin-r-10">
        <i class="fa fa-arrow-left"></i>
        <span class="kt-hidden-mobile">   {{ __('Back') }}</span>
    </a>

    <div class="btn-group float-right">
        @if(!$disabled)
        <button type="submit" class="btn btn-success" id="submitButton">
            <i class="fa fa-check"></i>
            <span class="kt-hidden-mobile">   {{ __('Save') }}</span>
        </button>
        @else
            <button type="button" class="btn btn-brand" onclick="window.location.href='{{route($route.'.edit',[$route=>$id])}}'">
                <i class="la la-pencil-square-o"></i>
                <span class="kt-hidden-mobile">   {{ __('Edit') }}</span>
            </button>
        @endif


    </div>
</div>
