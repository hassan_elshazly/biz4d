<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Project;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $status=[
            ['status', 'count'],
            [  'active',Project::where("status","active")->count()],
            [ 'incomplete',Project::where("status","incomplete")->count()],
            [ 'ongoing', Project::where("status","ongoing")->count()],
            [ 'stuck',Project::where("status","stuck")->count(),]
        ];
        $modules=[
            ['Month', 'projects', 'users'],
            [
                date("M", strtotime(date("Y-m-d")." -1 month")),
                Project::whereMonth('created_at', date("m", strtotime(date("Y-m-d")." +1 month")))->count(),
                User::whereMonth('created_at',date("m", strtotime(date("Y-m-d")." +1 month")))->count()
            ],
            [
                date("M"),
                Project::whereMonth('created_at', date("m"))->count(),
                User::whereMonth('created_at',date("m"))->count(),
            ],
            [
                date("M", strtotime(date("Y-m-d")." +2 month")),
                Project::whereMonth('created_at',date("m", strtotime(date("Y-m-d")." +2 month")))->count(),
                User::whereMonth('created_at', date("m", strtotime(date("Y-m-d")." +2 month")))->count()
            ]
        ];
        return view('dashboard',compact("status",'modules'));
    }
}
