<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;
use App\DataTables\ProjectDatatable;
use App\Http\Requests\StoreProjectRequest;
use App\Http\Requests\UpdateProjectRequest;
use App\Models\File;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()

    {
        $this->authorizeResource(Project::class, 'project');
    }
    public function index(ProjectDatatable $project)
    {

        $title=__('Project Management');
        return  $project->render("projects.index",compact("title"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title=__('Create Project');
        $status=Project::STATUS;
        return view("projects.create",compact("title","status"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProjectRequest $request)
    {
        $request->validated();
        $project=Project::create(array_merge(["user_id"=>auth()->id()],$request->all()));
        Project::files_attach($request,$project);
        session()->flash('created',__("Changes has been Created successfully"));
        if(auth()->user()->hasRole("user")){
            return redirect()->to(route("project.index")."?user=".auth()->id());
        }
        return redirect()->route("project.index");
    }

/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {

        $title=__('Show Project');
        $status=Project::STATUS;
        return view("projects.show",compact("project",'status'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        $title=__('Edit Project');
        $status=Project::STATUS;
        return view("projects.edit",compact("project",'status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProjectRequest $request, Project $project)
    {
        $request->validated();
        $project->update($request->all());
        Project::files_attach($request,$project,true);
        session()->flash('created',__("Changes has been Updated successfully"));
        if(auth()->user()->hasRole("user")){
            return redirect()->to(route("project.index")."?user=".auth()->id());
        }
        return redirect()->route("project.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        foreach($project->files as $file){
            delete_file($file);
            $file->delete();
        }
        $project->delete();
        session()->flash('deleted',__("Changes has been Deleted successfully"));
        return redirect()->route("project.index");
    }
}
