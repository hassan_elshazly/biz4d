<?php

namespace App\Http\Controllers;

use App\DataTables\UserDatatable;
use App\Models\User;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function index(UserDatatable $user)
    {
        $tite=__("User Management");
        return $user->render('users.index',compact("tite"));
    }
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title=__('Create User');
        return view("users.create",compact("title"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $request->validated();
        User::create($request->all());
        session()->flash('created',__("Changes has been Created successfully"));
        return redirect()->route("user.index");
    }

/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $title=__('Show User');
        return view("users.show",compact("user"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $title=__('Edit User');
        return view("users.edit",compact("user",));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        $request->validated();
        $user->update($request->all());
        session()->flash('created',__("Changes has been Updated successfully"));
        return redirect()->route("user.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->projects()->delete();
        $user->delete();
        session()->flash('deleted',__("Changes has been Deleted successfully"));
        return redirect()->route("user.index");
    }
}
