<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"    => "required|min:3",
            "desc"    => "required|min:3",
            "url"     => "required|url",
            "status"  => "required",
            "image"=> "required|image|mimes:jpeg,png,jpg,gif,svg|max:2048",
            "video"=> "required|mimes:mp4,ogx,oga,ogv,ogg,webm",
            "doc"  => "nullable|max:10000|mimes:doc,docx,xlsx,csv",
        ];
    }
}
