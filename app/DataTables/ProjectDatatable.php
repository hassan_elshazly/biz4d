<?php

namespace App\DataTables;

use App\Models\Project;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
class ProjectDatatable extends DataTable
{

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
          $datatable=datatables()
            ->eloquent($query)
            ->editColumn('name', '{{Str::limit($name, 100)}}')
            ->editColumn('status', '{{Str::limit($status, 100)}}')
            ->editColumn('desc', '{{Str::limit($desc, 100)}}')
            ->addColumn('checkbox', 'projects.btn.checkbox')
            ->addColumn('action', 'projects.btn.action')
            ->rawColumns(['checkbox', 'action']);
            if(!$this->request()->has("user")){
                $datatable->editColumn('user.name', function($q){
                    return view("projects.btn.user",["user"=>$q->user]);
                });
            }
        return $datatable;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\B  uilder
     */
    public function query()
    {
        if($this->request()->has("user")){
            return Project::query()->where("user_id",$this->request()->get("user"))->with(['files',"user"])->select("projects.*");
        }
        return Project::query()->with(['files',"user"])->select("projects.*");
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('Projects-table')
            ->columns($this->getColumns())
            ->dom('Bfrtip')
            ->parameters([
                'buttons'      => [
                    'pageLength',
                    [
                        "extend" => 'collection',
                        "text" => __("Export"),
                        "buttons" => ['csv', 'excel', 'print']
                    ],
                ],
                'lengthMenu' =>
                [
                    [10, 25, 50, -1],
                    ['10 ' . __('rows'), '25 ' . __('rows'), '50 ' . __('rows'), __('Show all')]
                ],

            ])
            ->minifiedAjax()
            ->orderBy(1)
            ->search([]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $columns= [

            [
                'name' => "checkbox",
                'data' => "checkbox",
                'title' =>
                "
                <label class='checkbox checkbox-single'>
                    <input type='checkbox'class='check_all' onclick='check_all()'/>
                    <span></span>
                </label>
                ",
                "exportable" => false,
                "printable" => false,
                "orderable" => false,
                "searchable" => false,
            ],
            Column::make('id'),
            Column::make('name')
                ->title(__("Name")),
            Column::make('status')
                ->title(__("Status")),
            Column::make('desc')
                ->title(__("Description")),
            Column::computed('action')
                ->title(__('Action'))
                ->exportable(false)
                ->printable(false)
                ->searchable(false)
                ->width(150)
                ->addClass('text-center')



        ];
        if(!$this->request()->has("user")){
            $columns[]=Column::make('user.name')
            ->title(__("User Name"));
        }
        return $columns;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Projects_' . date('YmdHis');
    }
}
