<?php

use App\Models\File;
use Illuminate\Support\Facades\Storage;


if (!function_exists("upload_file")) {
    function upload_file($file, $base,$object,$type)
    {

        $extension = $file->getClientOriginalExtension();
        $name = time() . rand(11111, 99999) . '.' . $extension;
        $firstCharacter = substr($base, 0, 1);
        if ($firstCharacter == '/') {
            $base = substr($base, 1);
        }else {
            $base = $base;
        }
        $lastCharacter = substr($base, -1, 1);
        if ($lastCharacter == '/') {
            $base = substr($base, 0, -1);
        }
        try {
            Storage::disk('public')->put($base .'/'. $name, file_get_contents($file));

            $object->files()->create(['name' => $name, 'base' => Storage::disk('public')->url($base . '/') ,'mime'=>$type]);
            return true;

        } catch (Exception $e) {
            return false;
        }

    }
}
if (!function_exists("delete_file")) {
    function delete_file($file)
    {
        try {
            Storage::disk('public')->delete($file->base.$file->name);
            $file->delete();
        } catch (Exception $e) {
            return false;
        }

    }
}



