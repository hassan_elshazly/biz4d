<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Project extends Model
{
    use HasFactory;
    protected $table="projects";
    protected $fillable=[
        "name",
        "desc",
        "url",
        "status",
        "user_id",
    ];
    const FILES_PATH_BASE="Project";
    const STATUS=['active'=>'active', 'incomplete'=>'incomplete', 'ongoing'=> 'ongoing','stuck'=>'stuck'];
    public function files(){
        return $this->morphMany(File::class,"fileable");
    }
    public function image(){
        return $this->files()->where("mime","image")->first();
    }
    public function video(){
        return $this->files()->where("mime","video")->first();
    }
    public function doc(){
        return $this->files()->where("mime","doc")->first();
    }
    public function user(){
        return $this->belongsTo(File::class,"user_id");
    }
    public static function files_attach($request,$project=null,$update=false){

        if($request->has("image") && !is_null($request->image)){
            upload_file($request->file('image'),Project::FILES_PATH_BASE,$project,'image');
            if($update){
                delete_file($project->image());
            }

        }
        if($request->has("video") && !is_null($request->video)){
            upload_file($request->file('video'),Project::FILES_PATH_BASE,$project,'video');
            if($update){
                delete_file($project->video());
            }
        }
        if($request->has("doc") && !is_null($request->doc)){
            upload_file($request->file('doc'),Project::FILES_PATH_BASE,$project,'doc');
            if($update){
                delete_file($project->doc());
            }
        }
    }
}
