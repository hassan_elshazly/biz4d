<?php

namespace Database\Seeders;

use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class FileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names=[
            "pro_1.jpg",
            "pro_2.jpg",
            "pro_3.jpg",
            "pro_4.jpg",
            "pro_5.jpg",
            "pro_6.png",
            "pro_7.jpeg",
            "pro_8.png",
            "pro_9.png",
            "pro_10.jpg",
        ];
        for($i=0;$i<10;$i++){
            \App\Models\File::create([
                "name" => $names[rand(0,9)],
                "base" => asset("/")."assets/img/projects/",
                "mime"=>"image",
                "fileable_id"=> Project::all()->random()->id,
                "fileable_type"=>Project::class,
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }
    }
}
