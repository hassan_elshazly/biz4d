<?php

namespace Database\Seeders;

use App\Models\File;
use App\Models\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker          = Faker::create();
        $status=['active', 'incomplete', 'ongoing','stuck'];
        for($i=0;$i<20;$i++){
            \App\Models\Project::create([
                "name"      => $faker->firstName,
                "desc"      => $faker->sentence,
                "url"       => $faker->url,
                "status"    => $status[rand(0,3)],
                "user_id"   => User::all()->random()->id,
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }

    }
}
